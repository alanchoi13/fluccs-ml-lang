use strict;
use warnings FATAL => 'all';
use Data::Dumper qw(Dumper);
use Cwd qw(abs_path);



my $d4j_home = $ENV{"D4J_HOME"};
my $defects4j_bin = $d4j_home."/framework/bin/";


foreach my $count (1 .. 65){
		print "======================= start Lang checkout b".$count." =====================\n";
		my $cmd_basic = "defects4j-fluccs 0 checkout -p Lang -v ".$count."b -w lang_".$count."_b";
		my $cmd_location = $defects4j_bin.$cmd_basic;
		system($cmd_location);
		print "======================= end Lang checkout b".$count." =====================\n";
}

foreach my $count (1 .. 65){
	print "======================= start Lang prepare b".$count." =====================\n";
	my $cmd_prepare = "defects4j-fluccs 1 fluccs-prepare -p Lang -b ".$count." -w lang_".$count."_b -c 1";
	my $cmd_location = $defects4j_bin.$cmd_prepare;
	system($cmd_location);
	print "======================= end Lang prepare b".$count." =====================\n";
}
