<project basedir="." default="compile" name="Lang">

    
    <import file="build_support.xml" /><property file="${user.home}/${component.name}.build.properties" />
    <property file="${user.home}/build.properties" />
    <property file="${basedir}/build.properties" />
    <property file="${basedir}/default.properties" />
    <property name="jdk.javadoc" value="http://download.oracle.com/javase/1.5.0/docs/api/" />
    <property name="collections.javadoc" value="http://commons.apache.org/collections/api-release/" />

    
    <path id="compile.classpath">
        <pathelement location="${build.home}/classes" />
    </path>

    
    <path id="test.classpath">
        <pathelement location="${build.home}/classes" />
        <pathelement location="${build.home}/tests" />
        <pathelement location="${junit.jar}" />
    	<pathelement location="${easymock.jar}" />
    	<pathelement location="${commons-io.jar}" />
    </path>

    
    <target description="Initialize and evaluate conditionals" name="init">
        <echo message="-------- ${component.name} ${component.version} --------" />
        <filter token="name" value="${component.name}" />
        <filter token="package" value="${component.package}" />
        <filter token="version" value="${component.version}" />
        <filter token="compile.source" value="${compile.source}" />
        <filter token="compile.target" value="${compile.target}" />
        <mkdir dir="${build.home}" />
    </target>

    
    <target depends="init" description="Compile shareable components" name="compile">
        <mkdir dir="${build.home}/classes" />
        <javac debug="${compile.debug}" deprecation="${compile.deprecation}" destdir="${build.home}/classes" encoding="${compile.encoding}" excludes="${compile.excludes}" includeantruntime="false" optimize="${compile.optimize}" source="${compile.source}" srcdir="${source.home}" target="${compile.target}">
            <classpath refid="compile.classpath" />
        </javac>
        <copy filtering="on" todir="${build.home}/classes">
            <fileset dir="${source.home}" excludes="**/*.java,**/*.html" />
        </copy>
    </target>

    <target depends="compile" description="Compile unit test cases" name="compile.tests">
        <mkdir dir="${build.home}/tests" />
        <javac debug="${compile.debug}" deprecation="off" destdir="${build.home}/tests" encoding="${compile.encoding}" includeantruntime="false" optimize="${compile.optimize}" source="${compile.source}" srcdir="${test.home}" target="${compile.target}">
            <classpath refid="test.classpath" />
        </javac>
        <copy filtering="on" todir="${build.home}/tests">
            <fileset dir="${test.home}" excludes="**/*.java" />
        </copy>
    </target>

    
    <target depends="compile.tests" description="Run all unit test cases" name="test">
        <echo message="Running unit tests ..." />
        <mkdir dir="${build.home}/test-reports" />
        <junit fork="yes" haltonfailure="${test.failonerror}" printsummary="true" showoutput="true">
            <classpath refid="test.classpath" />
            <formatter type="plain" usefile="true" />
            
            <test if="test.entry" name="${test.entry}" todir="${build.home}/test-reports" />
            <batchtest fork="yes" todir="${build.home}/test-reports" unless="test.entry">
                <fileset dir="${test.home}">
                    <include name="**/*Test.java" />
                    <exclude name="**/Abstract*Test.java" />
                </fileset>
            </batchtest>
        </junit>
    </target>

    <target description="Clean build and distribution directories" name="clean">
        <delete dir="${build.home}" />
    </target>

    <target depends="clean,test,compile" description="Clean and compile all components" name="all" />

    
    <target depends="compile" description="Create component Javadoc documentation" name="javadoc">
        <mkdir dir="${build.home}" />
        <mkdir dir="${build.home}/apidocs" />
        <tstamp>
            <format pattern="yyyy" property="current.year" />
        </tstamp>
        <javadoc author="false" bottom="Copyright &amp;copy; 2001-${current.year} - Apache Software Foundation" destdir="${build.home}/apidocs" doctitle="&lt;h1&gt;Commons Lang ${component.version}&lt;/h1&gt;" encoding="${compile.encoding}" excludepackagenames="${javadoc.excludepackagenames}" overview="${source.home}/org/apache/commons/lang3/overview.html" packagenames="org.apache.commons.*" source="${compile.source}" sourcepath="${source.home}" use="true" version="true" windowtitle="Lang ${component.version}">
            <classpath refid="compile.classpath" />
            <link href="${jdk.javadoc}" />
            <link href="${collections.javadoc}" />
        </javadoc>
    </target>

    
    <target depends="compile" description="Create jar" name="jar">
        <mkdir dir="${build.home}/classes/META-INF" />
        <copy file="LICENSE.txt" tofile="${build.home}/classes/META-INF/LICENSE.txt" />
        <copy file="NOTICE.txt" tofile="${build.home}/classes/META-INF/NOTICE.txt" />
        <jar jarfile="${build.home}/${final.name}.jar">
            <manifest>
                <attribute name="Specification-Title" value="Commons Lang" />
                <attribute name="Specification-Version" value="${component.version}" />
                <attribute name="Specification-Vendor" value="The Apache Software Foundation" />
                <attribute name="Implementation-Title" value="Commons Lang" />
                <attribute name="Implementation-Version" value="${component.version}" /> 
                <attribute name="Implementation-Vendor" value="The Apache Software Foundation" />
                <attribute name="Implementation-Vendor-Id" value="org.apache" />
                <attribute name="X-Compile-Source-JDK" value="${compile.source}" />
                <attribute name="X-Compile-Target-JDK" value="${compile.target}" />
            </manifest>
            <fileset dir="${build.home}/classes">
                <include name="**/*.class" />
                <include name="**/LICENSE.txt" />
                <include name="**/NOTICE.txt" />
            </fileset>
        </jar>
    </target>

    <target depends="javadoc" description="Create JavaDoc jar" name="javadoc-jar">
        <jar jarfile="${build.home}/${final.name}-javadoc.jar">
            <manifest>
                <attribute name="Specification-Title" value="Commons Lang API" />
                <attribute name="Specification-Version" value="${component.version}" />
                <attribute name="Specification-Vendor" value="The Apache Software Foundation" />
                <attribute name="Implementation-Title" value="Commons Lang API" />
                <attribute name="Implementation-Version" value="${component.version}" /> 
                <attribute name="Implementation-Vendor" value="The Apache Software Foundation" />
                <attribute name="Implementation-Vendor-Id" value="org.apache" />
            </manifest>
            <fileset dir="${build.home}/apidocs" />
            <fileset dir="${basedir}">
                <include name="LICENSE.txt" />
                <include name="NOTICE.txt" />
            </fileset>
        </jar>
    </target>

    <target depends="init" description="Create JavaDoc jar" name="source-jar">
        <jar jarfile="${build.home}/${final.name}-sources.jar">
            <manifest>
                <attribute name="Specification-Title" value="Commons Lang Source" />
                <attribute name="Specification-Version" value="${component.version}" />
                <attribute name="Specification-Vendor" value="The Apache Software Foundation" />
                <attribute name="Implementation-Title" value="Commons Lang Source" />
                <attribute name="Implementation-Version" value="${component.version}" /> 
                <attribute name="Implementation-Vendor" value="The Apache Software Foundation" />
                <attribute name="Implementation-Vendor-Id" value="org.apache" />
            </manifest>
            <fileset dir="${source.home}">
                <include name="**/*.java" />
            </fileset>
            <fileset dir="${basedir}">
                <include name="LICENSE.txt" />
                <include name="NOTICE.txt" />
            </fileset>
        </jar>
    </target>

    
    <target depends="clean,jar,source-jar,javadoc-jar" description="Create binary distribution" name="dist">

        
        <zip destfile="${build.home}/${final.name}.zip">
            <zipfileset dir="${basedir}" includes="LICENSE.txt,                                 NOTICE.txt,                                 RELEASE-NOTES.txt" prefix="${final.name}" />
            <zipfileset dir="${build.home}" includes="*.jar," prefix="${final.name}" />
            <zipfileset dir="${build.home}/apidocs" prefix="${final.name}/apidocs" />
        </zip>
        <tar compression="gzip" destfile="${build.home}/${final.name}.tar.gz">
            <zipfileset src="${build.home}/${final.name}.zip" />
        </tar>

        
        <zip destfile="${build.home}/${final.name}-src.zip">
            <zipfileset dir="${basedir}" includes="build.xml,                                 build.xml,                                 checkstyle.xml,                                 default.properties,                                 LICENSE.txt,                                 NOTICE.txt,                                 pom.xml,                                 RELEASE-NOTES.txt" prefix="${final.name}-src" />
            <zipfileset dir="${basedir}/src" prefix="${final.name}-src/src" />
        </zip>
        <tar compression="gzip" destfile="${build.home}/${final.name}-src.tar.gz">
            <zipfileset src="${build.home}/${final.name}-src.zip" />
        </tar>

    </target>
</project>